//#region constants
const TOKEN = process.env.TOKEN || ''
    , URL = process.env.URL || '127.0.0.1'
    , PORT = process.env.PORT || 4040
//#endregion

//#region import libs
const SDK = require('balebot')
    , ytdl = require('ytdl-core')
    , fs = require('fs')
//#endregion

//bot creation
let bot = new SDK.BaleBot(TOKEN)

let Conversation = SDK.Conversation,
    TemplateMessage = SDK.TemplateMessage,
    SimpleTemplate = SDK.SimpleTemplate,
    TextMessage = SDK.TextMessage,
    Button = SDK.ButtonElement

let download = new Conversation();

/*bot.hears(['hi'], (mes, res) => {
    fs.readFile('sample.jpg', function (err, imageBuffer) {
        if (err) {
            console.log(err)
            return
        }
        bot.UploadFile(imageBuffer, 'file').then(response => {
            var thumbBuffer = new Buffer(imageBuffer).toString('base64');
            let fileId = response.fileId;
            let fileAccessHash = response.accessHash;
            var photoMsg = new SDK.PhotoMessage(fileId, fileAccessHash.toString(), 'name.jpg', 10000, 'image/jpeg', 'caption', 640, 480, thumbBuffer);
            res.reply(photoMsg).then(() => {
                console.log("upload was successful")
            }).catch((err) => {
                console.log(err)
            });
        });
    })
})*/

download.startsWith(['/start', 'start', 'شروع'])
    .then((message, session, responder) => {
        responder.reply('لطفا لینک یوتیوب خود را ارسال کنید.')
        session.next()
    })
    .then((message, session, responder) => {
        responder.reply('چند لحظه صبر کنید، ربات در حال پردازش لینک است.')
        let url = message.text
        session.putToSessionMemory('url', url)
        let text = new TextMessage('کدام فرمت را میخواهید؟')
        formatButtons(url, (elements) => {
            let template = new TemplateMessage(new SimpleTemplate(text, elements, '1'))
            responder.reply(template)
        })
        session.next();
    })
    .then((message, session, responder) => {
        responder.reply('چند لحظه صبر کنید، ربات در حال پردازش لینک است.')
        let itag = message.text
        let url = session.getFromSessionMemory('url')
        /*let address, i = 0, fileId, fileHash, fileVer, reply
        while (fs.existsSync('./temp' + i + '.mp4'))
            i++
        address = './temp' + i + '.mp4'*/
        ytdl(url, { filter: (format) => format.itag === itag })
            .on('progress', (chunks, downloaded, downloadLength) => {
                console.log(downloadLength)
            })
        /*.on('info', (info, format) => {
            console.log(format);
        })*/
        /*.pipe(
            fs.createWriteStream(address)
        )
        .on('finish', () => {
            fs.readFile(address, (err, buffer) => {
                if (err) {
                    throw err
                }
                bot.UploadFile(buffer, 'file').then(response => {
                    // you can save the location of uploadedfile
                    console.log(response);
                    fileId = response.fileId
                    fileHash = response.accessHash
                    fileVer = response.version
                    //let file = SDK.VideoMessage(fileId, fileHash, address, size, 'mp4', 'none')
                }).catch((err) => {
                    console.error(err)
                    // you can do any thing with err :))))
                })
                fs.unlinkSync(address)
            })
        })*/
    })

download.cancelsWith(["/end", "/stop", "/bye"], (message, session, responder) => {
    responder.reply("OK. bye!");
});

bot.setConversation(download);

/*bot.setDefaultCallback((message, responder) => {
    let address, i = 0, fileId, fileHash, fileVer, reply
    while (fs.existsSync('./temp' + i + '.mp4')) 
        i++
    address = './temp' + i + '.mp4'
    let url = 'http://www.youtube.com/watch?v=A02s8omM_hI'
    let container = '360p'

    /*ytdl.getInfo(url, (err, info) => {
        if (err) throw err
        ytdl(url, { filter: (format) => format.container === container })
            .pipe(
                fs.createWriteStream(address)
            )
            .on('finish', () => {
                fs.readFile(address, (err, buffer) => {
                    if (err) {
                        throw err
                    }
                    bot.UploadFile(buffer, 'file').then(response => {
                        // you can save the location of uploadedfile
                        console.log(response);
                        fileId = response.fileId
                        fileHash = response.accessHash
                        fileVer = response.version
                        //let file = SDK.VideoMessage(fileId, fileHash, address, size, 'mp4', 'none')
                    }).catch((err) => {
                        console.error(err)
                        // you can do any thing with err :))))
                    })
                    fs.unlinkSync(address)
                })
            })
    })*/
/*});*/

function formatButtons(url, cb) {
    let formatButtons = []
    ytdl.getInfo(url, (err, info) => {
        if (err) throw err
        info.formats.forEach((format) => {
            let text = `${format.container} - ${(!format.resolution || format.resolution === null) ? (format.quality_label ? format.quality_label : format.type) : format.resolution} - ${format.size ? format.size : (format.quality ? format.quality : format.itag)}`
            let button = new Button(text, format.itag, 1)
            formatButtons.push(button)
        })
        cb(formatButtons)
    })
}

process.on('uncaughtException', function (err) {
    console.log(err);
}); 